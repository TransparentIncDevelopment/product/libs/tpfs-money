# tpfs-money

Currently supports `USD` and `EUR` in English only.  EUR exists primarily for testing compatibility between currencies

`yarn clean` - Remove `dist/`, `node_modules/`, and `coverage/`

`yarn lint` - Run `tslint`

`yarn test` - Run `jest` tests for `tpfs-money` with coverage report generated in `coverage/`

`yarn build` - Compile the project to `dist/` with `tsc`


# Installation

```bash
yarn add tpfs-money
```

or

```bash
yarn add tpfs-money@beta # Uses most recent Beta publish
```


# Usage

Basic usage:
```js
import Money from 'tpfs-money';
const OneDollar = new Money(100, 'USD');
const TwoDollars = new Money(200, 'USD');
```

This package has the ability to allocate funds from a source fund and return any remainder (usually 1 cent).  Allocation can be done evenly over a set number of resultant funds or distributed over a set number of resultant funds each given a percentage of the whole source.  Here is an example:
```js
const sourceFunds = new Money("10000");
const evenDistribution = sourceFunds.allocateEvenly(3);
// evenDistribution {
//      remainder: Money ($0.01)
//      allocatedFunds: [
//          Money ($33.33),
//          Money ($33.33),
//          Money ($33.33)
//      ]
// }
const manualDistribution = sourceFunds.allocate([33.33, 33.33]);
// manualDistribution {
//      remainder: Money ($33.34)
//      allocatedFunds: [
//          Money ($33.33),
//          Money ($33.33),
//      ]
// }
```

Operations done with Money objects return new Money objects.  Existing Money objects are not modified.  If an operation is invalid (e.g. trying to add a `EUR` to a `USD`) the operation will return `undefined`:
```js
const oneDollar = new Money(100, 'USD');
const oneEuro = new Money(100, 'EUR');
const addition = oneDollar.add(oneEuro); // undefined
```

# Notes

The amount printed in `en` locale can be obtained with .toString() or via template literals
```js
const oneDollarOneCent = new Money(101, 'USD');
const valueLabel = `${oneDollarOneCent}`; // "$1.01"
```

Precision is guaranteed for default cases defined by `bignumber.js` which is currently sufficient for our needs.  If we need to support much larger amounts of money in the future, we can expose this package's `bignumber.js` config and allow it to be configured by the consuming project.  

See `bignumber.js` documentation for more details: https://mikemcl.github.io/bignumber.js/#config

# Declaration Cheat-Sheet

```ts
declare class Money {
    static fromMajorUnits(amount: string | number | BigNumber, currency: Currency): Money;
    static displayMinorUnits(locale: string, currency: Currency, amountInMinorUnits: string | number | BigNumber): string;
    static minorToMajorUnits(amountInMinorUnits: number | string | BigNumber): BigNumber;
    static majorToMinorUnits(amountInMajorUnits: number | string | BigNumber): BigNumber;
    readonly amountInMinorUnits: BigNumber;
    readonly currency: Currency;
    get amountInMajorUnits(): BigNumber;
    get minorUnitsString(): string;
    get majorUnitsString(): string;
    get decimalString(): string;
    get isZero(): boolean;
    constructor(amountInMinorUnits: string | number | BigNumber, currency: Currency);
    toString(): string;
    equals(other: Money): boolean;
    compareTo(other: Money): -1 | 0 | 1 | undefined;
    isCompatibleWith(other: Money): boolean;
    add(other: Money): Money | undefined;
    addAmount(amountInMinorUnits: string | number | BigNumber): Money;
    subtract(other: Money): Money | undefined;
    subtractAmount(amountInMinorUnits: string | number | BigNumber): Money | undefined;
    multiply(other: Money): Money | undefined;
    multiplyByAmount(multiplier: string | number | BigNumber): Money;
    allocateEvenly(allocations: number): MoneyAllocationResult;
    allocate(allocationPercentages: number[]): MoneyAllocationResult | undefined;
    private applyMathIfCompatible;
    private allocationDistribution;
}
```
