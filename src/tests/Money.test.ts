import BigNumber from 'bignumber.js';
import fc from 'fast-check';

import Money from '../Money';

describe('Money', () => {
  describe('Money.displayDecimal', () => {
    it('shows in decimal format', () => {
      expect(Money.displayDecimal('en-US', 100)).toEqual('1.00');
      expect(Money.displayDecimal('en-US', 325)).toEqual('3.25');
    });

    it('formats a valid number string', () => {
      const value = '1.00';
      expect(Money.displayDecimal('en-US', value)).toEqual('0.01');
    });

    it('skips formatting an invalid number string', () => {
      const value = 'NaN!!';
      expect(Money.displayDecimal('en-US', value)).toEqual(value);
    });

    it('formats a BigNumber', () => {
      const value = new BigNumber(100);
      expect(Money.displayDecimal('en-US', value)).toEqual('1.00');
    });
  });

  describe('Money.displayCurrency', () => {
    it('shows in currency format', () => {
      expect(Money.displayCurrency('en-US', 'USD', 100)).toEqual('$1.00');
      expect(Money.displayCurrency('en-US', 'USD', 325)).toEqual('$3.25');
    });

    it('formats a valid number string', () => {
      const value = '1.00';
      expect(Money.displayCurrency('en-US', 'USD', value)).toEqual('$0.01');
    });

    it('skips formatting an invalid number string', () => {
      const value = 'NaN!!';
      expect(Money.displayCurrency('en-US', 'USD', value)).toEqual(value);
    });

    it('formats a BigNumber', () => {
      const value = new BigNumber(100);
      expect(Money.displayCurrency('en-US', 'USD', value)).toEqual('$1.00');
    });
  });

  describe('Money.minorToMajorUnits', () => {
    it('converts minor units to major units', () => {
      const minorUnits = 100;
      const majorUnits = Money.minorToMajorUnits(minorUnits);
      expect(majorUnits.toFixed()).toEqual('1');
    });

    it('converts tricky minor units to major units', () => {
      const minorUnits = 16382758348327;
      const majorUnits = Money.minorToMajorUnits(minorUnits);
      expect(majorUnits.toFixed()).toEqual('163827583483.27');
    });

    it('converts 999 trillion minor units to major units', () => {
      const minorUnits = 999999999999999;
      const majorUnits = Money.minorToMajorUnits(minorUnits);
      expect(majorUnits.toFixed()).toEqual('9999999999999.99');
    });

    it('converts fractional minor units to major units', () => {
      const minorUnits = 99.99;
      const majorUnits = Money.minorToMajorUnits(minorUnits);
      expect(majorUnits.toFixed()).toEqual('0.9999');
    });

    it('converts negative minor units to major units', () => {
      const minorUnits = -99.99;
      const majorUnits = Money.minorToMajorUnits(minorUnits);
      expect(majorUnits.toFixed()).toEqual('-0.9999');
    });
  });

  describe('Money.majorToMinorUnits', () => {
    it('converts major units to minor units', () => {
      const majorUnits = 1.00;
      const minorUnits = Money.majorToMinorUnits(majorUnits);
      expect(minorUnits.toFixed()).toEqual('100');
    });

    it('converts tricky major units to minor units', () => {
      const majorUnits = 42975.64;
      const minorUnits = Money.majorToMinorUnits(majorUnits);
      expect(minorUnits.toFixed()).toEqual('4297564');
    });

    it('handles floating point precision accurately', () => {
      const majorUnit = 2.22;
      const minorUnit = Money.majorToMinorUnits(majorUnit);
      expect(minorUnit.toNumber()).toEqual(222);
    });

    it('converts 9.99 trillion major units to minor units', () => {
      const majorUnits = 9999999999999.99;
      const minorUnits = Money.majorToMinorUnits(majorUnits);
      expect(minorUnits.toFixed()).toEqual('999999999999999');
    });

    it('converts fractional major units to minor units', () => {
      const majorUnits = 12.09;
      const minorUnits = Money.majorToMinorUnits(majorUnits);
      expect(minorUnits.toFixed()).toEqual('1209');
    });

    it('converts negative major units to minor units', () => {
      const majorUnits = -12.09;
      const minorUnits = Money.majorToMinorUnits(majorUnits);
      expect(minorUnits.toFixed()).toEqual('-1209');
    });
  });

  describe('Money.fromMajorUnits', () => {
    it('converts major units into a money object', () => {
      const money = Money.fromMajorUnits(237.27, 'USD');
      expect(money.isZero).toBeFalsy();
      expect(money.minorUnitsString).toEqual('23727');
      expect(money.majorUnitsString).toEqual('237.27');
      expect(`${money}`).toEqual('$237.27');
    });

    it('rounds Money value when creating money with inappropriate decimals (half up)', () => {
      const money = Money.fromMajorUnits(237.275555, 'USD');
      expect(money.isZero).toBeFalsy();
      expect(money.minorUnitsString).toEqual('23728');
      expect(money.majorUnitsString).toEqual('237.28');
      expect(`${money}`).toEqual('$237.28');
    });

    it('rounds Money value when creating money with inappropriate decimals (down)', () => {
      const money = Money.fromMajorUnits(237.231111111, 'USD');
      expect(money.isZero).toBeFalsy();
      expect(money.minorUnitsString).toEqual('23723');
      expect(money.majorUnitsString).toEqual('237.23');
      expect(`${money}`).toEqual('$237.23');
    });

    it('Throws if an invalid major units amount is given', () => {
      const moneyFn = () => Money.fromMajorUnits('notevenanumberlol', 'USD');
      expect(moneyFn).toThrowError();
    });
  });

  it('creates a Money', () => {
    const money = new Money(100, 'USD');
    expect(money.currency).toEqual('USD');
    expect(money.amountInMinorUnits.isEqualTo(100)).toBeTruthy();
    expect(money.amountInMajorUnits.isEqualTo(1)).toBeTruthy();
    expect(money.isZero).toBeFalsy();
    expect(money.minorUnitsString).toEqual('100');
    expect(money.decimalString).toEqual('1.00');
    expect(money.majorUnitsString).toEqual('1');
    expect(money.toString('en-US')).toEqual('$1.00');
  });

  it('prints a currency string when used in string', () => {
    const money = new Money(100, 'USD');
    expect(`${money}`).toEqual('$1.00');
  });

  it('Throws error when creating Money from an invalid value', () => {
    const moneyFn = () => new Money('Final Fantasy 7 is overrated', 'USD');
    expect(moneyFn).toThrowError();
  });

  it('compares equal Money', () => {
    const moneyOne = new Money('100', 'USD');
    const moneyTwo = new Money(100, 'USD');
    expect(moneyOne.equals(moneyTwo)).toBeTruthy();
  });

  it('compares inequal Money', () => {
    const moneyOne = new Money('100', 'USD');
    const moneyTwo = new Money(101, 'USD');
    expect(moneyOne.equals(moneyTwo)).toBeFalsy();
  });

  it('compares incompatible Money', () => {
    const moneyOne = new Money('100', 'USD');
    const moneyTwo = new Money(100, 'EUR');
    expect(moneyOne.equals(moneyTwo)).toBeFalsy();
    expect(moneyOne.isCompatibleWith(moneyTwo)).toBeFalsy();
  });

  it('adds compatible Money', () => {
    const moneyOne = new Money('100', 'USD');
    const moneyTwo = new Money(101, 'USD');
    const moneySum = moneyOne.add(moneyTwo);
    expect(`${moneySum}`).toEqual('$2.01');
  });

  it('returns undefined when adding incompatible money', () => {
    const moneyOne = new Money('100', 'USD');
    const moneyTwo = new Money(100, 'EUR');
    const moneySum = moneyOne.add(moneyTwo);
    expect(moneySum).toBeUndefined();
  });

  it('returns undefined when comparing incompatible money', () => {
    const moneyOne = new Money('100', 'USD');
    const moneyTwo = new Money(100, 'EUR');
    const moneyCompare = moneyOne.compareTo(moneyTwo);
    expect(moneyCompare).toBeUndefined();
  });

  it('returns 0 when comparing equal money', () => {
    const moneyOne = new Money('100', 'USD');
    const moneyTwo = new Money(100, 'USD');
    const moneyCompare = moneyOne.compareTo(moneyTwo);
    expect(moneyCompare).toBe(0);
  });

  it('returns -1 when comparing equal money it is less than', () => {
    const moneyOne = new Money('100', 'USD');
    const moneyTwo = new Money(101, 'USD');
    const moneyCompare = moneyOne.compareTo(moneyTwo);
    expect(moneyCompare).toBe(-1);
  });

  it('returns 1 when comparing equal money it is greater than', () => {
    const moneyOne = new Money('100', 'USD');
    const moneyTwo = new Money(99, 'USD');
    const moneyCompare = moneyOne.compareTo(moneyTwo);
    expect(moneyCompare).toBe(1);
  });

  it('adds a minor unit amount to a Money', () => {
    const money = new Money('100', 'USD');
    const moneyAdded = money.addAmount(20212);
    expect(`${moneyAdded}`).toEqual('$203.12');
  });

  it('subtracts compatible Money', () => {
    const moneyOne = new Money('100', 'USD');
    const moneyTwo = new Money(99, 'USD');
    const moneyDiff = moneyOne.subtract(moneyTwo);
    expect(`${moneyDiff}`).toEqual('$0.01');
  });

  it('returns undefined when subtracting incompatible money', () => {
    const moneyOne = new Money('100', 'USD');
    const moneyTwo = new Money(1, 'EUR');
    const moneyDiff = moneyOne.subtract(moneyTwo);
    expect(moneyDiff).toBeUndefined();
  });

  it('subtracts a minor unit amount to a Money', () => {
    const money = new Money('100', 'USD');
    const moneySubtracted = money.subtractAmount(20);
    expect(`${moneySubtracted}`).toEqual('$0.80');
  });

  it('returns negative when subtracting to a negative amount', () => {
    const moneyOne = new Money('100', 'USD');
    const moneyTwo = new Money(1000, 'USD');
    const moneyDiff = moneyOne.subtract(moneyTwo);
    expect(`${moneyDiff}`).toEqual('-$9.00');
  });

  it('multiplies money by the major value of compatible money', () => {
    const money = new Money(100, 'USD');
    const moneyTwo = new Money(200, 'USD');
    const moneyProduct = money.multiply(moneyTwo);
    expect(`${moneyProduct}`).toEqual('$2.00');
  });

  it('multiplies money by the major value of compatible money with proper rounding', () => {
    const money = Money.fromMajorUnits(100.87, 'USD');
    const moneyTwo = new Money(201, 'USD');
    const moneyProduct = money.multiply(moneyTwo);
    // rounded from '202.7487'
    expect(`${moneyProduct}`).toEqual('$202.75');
  });

  it('returns undefined when multiplying incompatible money values', () => {
    const money = Money.fromMajorUnits(100.87, 'USD');
    const moneyTwo = new Money(201, 'EUR');
    const moneyProduct = money.multiply(moneyTwo);
    expect(moneyProduct).toBeUndefined();
  });

  it('multiplies money by a given amount (and rounds down)', () => {
    const money = new Money('103', 'USD');
    const moneyProduct = money.multiplyByAmount(7.77);
    // rounded from 800.31 minor units
    expect(`${moneyProduct}`).toEqual('$8.00');
  });

  it('multiplies money by a given amount (and rounds up at 5)', () => {
    const money = new Money('103', 'USD');
    const moneyProduct = money.multiplyByAmount(7.85);
    // rounded from 808.55 minor units
    expect(`${moneyProduct}`).toEqual('$8.09');
  });

  it('allocates money evenly between a set number', () => {
    const funds = new Money(9999, 'USD');
    const allocation = funds.allocateEvenly(3);
    expect(allocation.remainder.isZero).toBeTruthy();
    expect(allocation.allocatedFunds.length).toEqual(3);
    expect(`${allocation.allocatedFunds[0]}`).toEqual('$33.33');
    expect(`${allocation.allocatedFunds[1]}`).toEqual('$33.33');
    expect(`${allocation.allocatedFunds[2]}`).toEqual('$33.33');
  });

  it('allocates money evenly between a set number with remainder', () => {
    const funds = new Money(10000, 'USD');
    const allocation = funds.allocateEvenly(3);
    expect(`${allocation.remainder}`).toEqual('$0.01');
    expect(allocation.allocatedFunds.length).toEqual(3);
    expect(`${allocation.allocatedFunds[0]}`).toEqual('$33.33');
    expect(`${allocation.allocatedFunds[1]}`).toEqual('$33.33');
    expect(`${allocation.allocatedFunds[2]}`).toEqual('$33.33');
  });

  it('allocates money with given percentages', () => {
    const funds = new Money(10000, 'USD');
    const allocationPercentages = [10, 10, 10, 10, 10, 50];
    const allocation = funds.allocate(allocationPercentages);
    expect(allocation === undefined).toBeFalsy();
    expect(allocation!.remainder.isZero).toBeTruthy();
    expect(allocation!.allocatedFunds.length).toEqual(6);
    expect(`${allocation!.allocatedFunds[0]}`).toEqual('$10.00');
    expect(`${allocation!.allocatedFunds[1]}`).toEqual('$10.00');
    expect(`${allocation!.allocatedFunds[2]}`).toEqual('$10.00');
    expect(`${allocation!.allocatedFunds[3]}`).toEqual('$10.00');
    expect(`${allocation!.allocatedFunds[4]}`).toEqual('$10.00');
    expect(`${allocation!.allocatedFunds[5]}`).toEqual('$50.00');
  });

  it('allocates money with given percentages with remainder', () => {
    const funds = new Money(10000, 'USD');
    const allocationPercentages = [33.33, 33.33, 33.33];
    const allocation = funds.allocate(allocationPercentages);
    expect(allocation === undefined).toBeFalsy();
    expect(`${allocation!.remainder}`).toEqual('$0.01');
    expect(allocation!.allocatedFunds.length).toEqual(3);
    expect(`${allocation!.allocatedFunds[0]}`).toEqual('$33.33');
    expect(`${allocation!.allocatedFunds[1]}`).toEqual('$33.33');
    expect(`${allocation!.allocatedFunds[2]}`).toEqual('$33.33');
  });

  it('allocates partial money with given percentages with remainder', () => {
    const funds = new Money(10000, 'USD');
    const allocationPercentages = [33.33, 33.33];
    const allocation = funds.allocate(allocationPercentages);
    expect(allocation === undefined).toBeFalsy();
    expect(`${allocation!.remainder}`).toEqual('$33.34');
    expect(allocation!.allocatedFunds.length).toEqual(2);
    expect(`${allocation!.allocatedFunds[0]}`).toEqual('$33.33');
    expect(`${allocation!.allocatedFunds[1]}`).toEqual('$33.33');
  });

  it('returns undefined when attempting to allocate more than 100%', () => {
    const funds = new Money(10000, 'USD');
    const allocationPercentages = [33.33, 33.33, 33.33, .10];
    const allocation = funds.allocate(allocationPercentages);
    expect(allocation).toBeUndefined();
  });

  it('returns undefined when attempting to allocate a negative amount', () => {
    const funds = new Money(10000, 'USD');
    const allocationPercentages = [33.33, -33.33, 33.33];
    const allocation = funds.allocate(allocationPercentages);
    expect(allocation).toBeUndefined();
  });

  describe('Money Prop Tests', () => {
    it('should always result in a valid money string', () => {
      fc.assert(fc.property(fc.integer(Number.MAX_SAFE_INTEGER), (minorUnit) => {
        const funds = new Money(minorUnit, 'USD');
        return funds.majorUnitsString.search(/^\s*-?\d+(\.\d{1,2})?\s*$/) > -1;
      }));
    });

    it('will always equal another Money with the same value and currency', () => {
      fc.assert(fc.property(
        fc.integer(0, Number.MAX_SAFE_INTEGER),
        (minorUnit) => {
          const fundsOne = new Money(minorUnit, 'USD');
          const fundsTwo = new Money(minorUnit, 'USD');
          return fundsOne.equals(fundsTwo);
        }));
    });

    describe('Arithmetic Tests', () => {
      it('always results in a valid value when adding', () => {
        fc.assert(fc.property(
          fc.integer(Number.MAX_SAFE_INTEGER),
          fc.integer(Number.MAX_SAFE_INTEGER),
          (minorUnitOne, minorUnitTwo) => {
            const fundsOne = new Money(minorUnitOne, 'USD');
            const fundsTwo = new Money(minorUnitTwo, 'USD');
            const funds = fundsOne.add(fundsTwo);
            return funds!.majorUnitsString.search(/^\s*-?\d+(\.\d{1,2})?\s*$/) > -1;
          }));
      });

      it('only returns true for isZero with 0', () => {
        fc.assert(fc.property(
          fc.integer(),
          (minorUnit) => {
            const funds = new Money(minorUnit, 'USD');
            return (funds.isZero && minorUnit === 0) || (!funds.isZero && minorUnit !== 0);
          }));
      });

      it('always results in a valid value when subtracting', () => {
        fc.assert(fc.property(
          fc.integer(Number.MAX_SAFE_INTEGER),
          fc.integer(Number.MAX_SAFE_INTEGER),
          (minorUnitOne, minorUnitTwo) => {
            const fundsOne = new Money(minorUnitOne, 'USD');
            const fundsTwo = new Money(minorUnitTwo, 'USD');
            const funds = fundsOne.subtract(fundsTwo);
            return funds!.majorUnitsString.search(/^\s*-?\d+(\.\d{1,2})?\s*$/) > -1;
          }));
      });

      it('always results in a valid value when multiplying', () => {
        fc.assert(fc.property(
          fc.integer(Number.MAX_SAFE_INTEGER),
          fc.integer(Number.MAX_SAFE_INTEGER),
          (minorUnitOne, minorUnitTwo) => {
            const fundsOne = new Money(minorUnitOne, 'USD');
            const fundsTwo = new Money(minorUnitTwo, 'USD');
            const funds = fundsOne.multiply(fundsTwo);
            return funds!.majorUnitsString.search(/^\s*-?\d+(\.\d{1,2})?\s*$/) > -1;
          }));
      });

      it('should always allocate evenly in allocateEvenly', () => {
        fc.assert(fc.property(fc.integer(), fc.integer(0, 2500), (minorUnit, allocateAmnt) => {
          const funds = new Money(minorUnit, 'USD');
          const allocation = funds.allocateEvenly(allocateAmnt);
          const values: {[key: string]: boolean} = {};
          allocation.allocatedFunds.forEach((fund: Money) => {
            values[fund.minorUnitsString] = true;
          });
          return (
            Object.keys(values).length === 1 ||
            (
              allocateAmnt === 0 &&
              allocation.remainder.amountInMinorUnits.toNumber() === minorUnit
            )
          );
        }));
      });
    });
  });
});
