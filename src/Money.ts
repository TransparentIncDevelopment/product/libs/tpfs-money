import {BigNumber} from 'bignumber.js';

import Currency from './types/Currency';
import MoneyAllocationResult from './interfaces/MoneyAllocationResult';

const DOLLAR_TO_CENTS = 100.00;

// NOTE: When rounding with money, always round to the nearest cent; 5 rounds up.
// bignumber.js does this by default.
class Money {
    public static fromMajorUnits(amount: string | number | BigNumber, currency: Currency) {
        const initAmount = new BigNumber(amount);
        if (initAmount.isNaN()) {
            throw new Error(`${amount} is an invalid amount value.`);
        }
        return new Money(Money.majorToMinorUnits(amount), currency);
    }

    public static displayDecimal(
        locale: string,
        amountInMinorUnits: string | number | BigNumber,
    ) {
        const val = new BigNumber(amountInMinorUnits);
        if (val.isNaN()) {
            return amountInMinorUnits.toString();
        }
        const inMajorUnit = Money.minorToMajorUnits(val.toNumber());
        return Intl.NumberFormat(locale, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })
          .format(inMajorUnit.toNumber());
    }

    public static displayCurrency(
        locale: string,
        currency: Currency,
        amountInMinorUnits: string | number | BigNumber,
    ) {
        const val = new BigNumber(amountInMinorUnits);
        if (val.isNaN()) {
            return amountInMinorUnits.toString();
        }
        const inMajorUnit = Money.minorToMajorUnits(val.toNumber());
        return Intl.NumberFormat(locale, {
            style: 'currency',
            currency,
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })
          .format(inMajorUnit.toNumber());
    }

    public static minorToMajorUnits(amountInMinorUnits: number | string | BigNumber) {
        const val = new BigNumber(amountInMinorUnits);
        return val.dividedBy(DOLLAR_TO_CENTS);
    }

    public static majorToMinorUnits(amountInMajorUnits: number | string | BigNumber) {
        const val = new BigNumber(amountInMajorUnits);
        return val.multipliedBy(DOLLAR_TO_CENTS);
    }

    public readonly amountInMinorUnits: BigNumber;

    public readonly currency: Currency;

    get amountInMajorUnits(): BigNumber {
        return Money.minorToMajorUnits(this.amountInMinorUnits);
    }

    get minorUnitsString(): string {
        return this.amountInMinorUnits.toFixed();
    }

    get majorUnitsString(): string {
        return this.amountInMajorUnits.toFixed();
    }

    get decimalString(): string {
        return this.amountInMajorUnits.toFixed(2);
    }

    get isZero(): boolean {
        return this.amountInMinorUnits.isEqualTo(0);
    }

    constructor(amountInMinorUnits: string | number | BigNumber, currency: Currency) {
        const initAmount = new BigNumber(amountInMinorUnits).decimalPlaces(0);
        if (initAmount.isNaN()) {
            throw new Error(`${amountInMinorUnits} is an invalid amount value.`);
        }
        this.amountInMinorUnits = initAmount;
        this.currency = currency;
    }

    public toString(locale: string = 'en-US'): string {
        // NOTE: To support multiple languages / locales, adapt function to take in locale string
        return Money.displayCurrency(locale, this.currency, this.amountInMinorUnits);
    }

    public equals(other: Money): boolean {
        return this.amountInMinorUnits.isEqualTo(other.amountInMinorUnits) && this.currency === other.currency;
    }

    public compareTo(other: Money): -1 | 0 | 1 | undefined {
        if (this.equals(other)) {
            return 0;
        }
        if (this.currency !== other.currency) {
            return undefined;
        }
        if (this.amountInMinorUnits.isGreaterThan(other.amountInMinorUnits)) {
            return 1;
        }
        return -1;
    }

    public isCompatibleWith(other: Money): boolean {
        return this.currency === other.currency;
    }

    public add(other: Money): Money | undefined {
        const addFn = () => this.addAmount(other.amountInMinorUnits);
        return this.applyMathIfCompatible(other, addFn);
    }

    public addAmount(amountInMinorUnits: string | number | BigNumber): Money {
        return new Money(this.amountInMinorUnits.plus(amountInMinorUnits).decimalPlaces(0), this.currency);
    }

    public subtract(other: Money): Money | undefined {
        const subtractFn = () => this.subtractAmount(other.amountInMinorUnits);
        return this.applyMathIfCompatible(other, subtractFn);
    }

    public subtractAmount(amountInMinorUnits: string | number | BigNumber): Money | undefined {
        return new Money(this.amountInMinorUnits.minus(amountInMinorUnits).decimalPlaces(0), this.currency);
    }

    public multiply(other: Money): Money | undefined {
        const multiplyFn = () => this.multiplyByAmount(other.amountInMajorUnits);
        return this.applyMathIfCompatible(other, multiplyFn);
    }

    public multiplyByAmount(multiplier: string | number | BigNumber): Money {
        return new Money(this.amountInMinorUnits.multipliedBy(multiplier).decimalPlaces(0), this.currency);
    }

    public allocateEvenly(allocations: number): MoneyAllocationResult {
        if (allocations <= 0) {
            return {allocatedFunds: [], remainder: this};
        }
        const allocationSplit = 100.00 / allocations;
        const allocationPercentages: number[] = Array(allocations).fill(allocationSplit);
        return this.allocationDistribution(allocationPercentages);
    }

    public allocate(allocationPercentages: number[]): MoneyAllocationResult | undefined {
        let totalAllocationPercent = 0;
        let negativePercentFound = false;
        allocationPercentages.forEach((alloc: number) => {
            negativePercentFound = negativePercentFound ? negativePercentFound : alloc < 0;
            totalAllocationPercent += alloc;
        });
        if (totalAllocationPercent > 100.00 || negativePercentFound) {
            return undefined;
        }
        return this.allocationDistribution(allocationPercentages);
    }

    private applyMathIfCompatible(other: Money, mathFn: () => Money | undefined) {
        if (!this.isCompatibleWith(other)) {
            return undefined;
        }
        return mathFn();
    }

    private allocationDistribution(allocationPercentages: number[]): MoneyAllocationResult {
        const allocatedFunds: Money[] = [];
        let remainderAmount = this.amountInMinorUnits;
        allocationPercentages.forEach((alloc: number) => {
            const allocationPercent = alloc / 100.00;
            const allocationAmount = this.amountInMinorUnits.multipliedBy(allocationPercent);
            const allocation = new Money(allocationAmount, this.currency);
            // subtract the new money amount rather than the allocationAmount to obtain remainder after rounding Monies
            remainderAmount = remainderAmount.minus(allocation.amountInMinorUnits);
            allocatedFunds.push(allocation);
        });
        const remainder = new Money(remainderAmount, this.currency);
        return {remainder, allocatedFunds};
    }
}

export default Money;
